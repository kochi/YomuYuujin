from pathlib import Path

from loguru import logger
from natsort import natsorted
from tqdm import tqdm

from .manga_page_ocr import MangaPageOcr
from .utils import dump_json
from os.path import basename


class OverlayGenerator:
    def __init__(self,
                 pretrained_model_name_or_path='kha-white/manga-ocr-base',
                 force_cpu=False,
                 force_ocr=False,
                 **kwargs):
        self.pretrained_model_name_or_path = pretrained_model_name_or_path
        self.force_cpu = force_cpu
        self.kwargs = kwargs
        self.mpocr = None
        self.force_ocr = force_ocr

    def init_models(self, qt_callback=False):
        if self.mpocr is None:
            self.mpocr = MangaPageOcr(self.pretrained_model_name_or_path, self.force_cpu, qt_callback=qt_callback,
                                      **self.kwargs)

    def process_dir(self, path, as_one_file=True, is_demo=False, force_ocr=False, qt_callback=False):
        path = Path(path).expanduser().absolute()
        assert path.is_dir(), f'{path} must be a directory'
        if path.stem == '_ocr':
            logger.info(f'Skipping OCR directory: {path}')
            return
        out_dir = path.parent

        results_dir = out_dir / '_ocr' / path.name
        results_dir.mkdir(parents=True, exist_ok=True)

        img_paths = [p for p in natsorted(path.glob('**/*')) if
                     p.is_file() and p.suffix.lower() in ('.jpg', '.jpeg', '.png')]

        page_htmls = []

        # for img_path in tqdm(img_paths, desc='Processing pages...'):
        for count, img_path in enumerate(img_paths):
            json_path = (results_dir / img_path.relative_to(path)).with_suffix('.json')
            if json_path.is_file() and not self.force_ocr:
                logger.info(f'Skipping, founded previous OCR for {img_path}')
                if qt_callback:
                    qt_callback.emit(
                        f"[{round(count / len(img_paths) * 100)}%] Skipping, founded previous OCR for {basename(img_path)}")
            else:
                self.init_models(qt_callback=qt_callback)
                result = self.mpocr(img_path)
                json_path.parent.mkdir(parents=True, exist_ok=True)
                dump_json(result, json_path)
                if qt_callback:
                    qt_callback.emit(f"[{round(count / len(img_paths) * 100)}%] Finished {basename(img_path)}")
