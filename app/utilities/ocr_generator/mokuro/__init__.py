__version__ = '0.1.3'

from .manga_page_ocr import MangaPageOcr
from .overlay_generator import OverlayGenerator

from .run import run
