import shutil
import zipfile
from os.path import basename
from pathlib import Path

from loguru import logger
from natsort import natsorted

from . import OverlayGenerator


def run(*paths,
        parent_dir=None,
        pretrained_model_name_or_path='kha-white/manga-ocr-base',
        force_cpu=False,
        force_ocr=False,
        qt_progress_callback=False
        ):

    paths = [Path(p).expanduser().absolute() for p in paths]

    if parent_dir is not None:
        for p in Path(parent_dir).expanduser().absolute().iterdir():
            if p.is_dir() and p.stem != '_ocr' and p not in paths:
                paths.append(p)

    if len(paths) == 0:
        logger.error('Found no paths to process. Did you set the paths correctly?')
        if qt_progress_callback:
            qt_progress_callback.emit('Found no paths to process. Did you set the paths correctly?')
        return

    paths = natsorted(paths)

    print(f'\nPaths to process:\n')
    for p in paths:
        print(p)

    ovg = OverlayGenerator(pretrained_model_name_or_path=pretrained_model_name_or_path, force_cpu=force_cpu,
                           force_ocr=force_ocr)

    num_sucessful = 0
    for i, path in enumerate(paths):
        logger.info(f'Processing {i + 1}/{len(paths)}: {path}')
        if qt_progress_callback:
            qt_progress_callback.emit(f'<p><span style="color: blue;">Processing {i + 1}/{len(paths)}: {basename(path)} </span>')
        try:
            # CBZ Handling
            cbz_done = False
            if path.match('*.cbz') or path.match('*.zip'):
                logger.info(f'Detected CBZ, Extracting {path}')
                if qt_progress_callback:
                    qt_progress_callback.emit(f'Detected CBZ, Extracting {basename(path)}')
                with zipfile.ZipFile(path, 'r') as zip_ref:
                    zip_ref.extractall(path.with_suffix(''))
                path = path.with_suffix('')
                cbz_done = True

            ovg.process_dir(path, qt_callback=qt_progress_callback)

            # Remove extracted directory after done, to save space
            if cbz_done:
                shutil.rmtree(path)
        except Exception:
            logger.exception(f'Error while processing {path}')
            if qt_progress_callback:
                qt_progress_callback.emit(f'<p><span style="color: red;">Error while processing {basename(path)} </span>')
        else:
            num_sucessful += 1

    logger.info(f'Processed successfully: {num_sucessful}/{len(paths)}')
    if qt_progress_callback:
        qt_progress_callback.emit(f'<p><span style="color: green;">Processed successfully: {num_sucessful}/{len(paths)} </span>')
