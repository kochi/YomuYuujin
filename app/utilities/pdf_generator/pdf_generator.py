import json
import os
import pathlib

import fitz
from PIL import Image
from natsort import natsorted


class PDFGenerator:
    def __init__(self, path_in, conversion_type, output_path, ocr=True):
        # type is either, parent or target
        self.conversion_type = conversion_type
        self.output_path = output_path
        self.ocr = ocr

        if self.conversion_type == "parent":
            self.path = pathlib.Path(path_in)
            self.ocr_dir = pathlib.Path(os.path.join(os.path.join(path_in, "_ocr/")))
            self.ocr_files = {}
            self.image_files = {}

            if self.ocr:
                self.volumes = natsorted([x for x in self.ocr_dir.iterdir() if x.is_dir() and x.name != "_ocr"])
                for vol in self.volumes:
                    p_o = pathlib.Path(os.path.join(os.path.join(path_in, "_ocr/"), vol.name))
                    p_i = pathlib.Path(os.path.join(path_in, vol.name))

                    self.ocr_files[p_o] = natsorted([x for x in p_o.iterdir() if x.is_file()])
                    self.image_files[p_i] = natsorted(
                        [x for x in p_i.iterdir() if x.is_file() and x.suffix.lower() in ('.jpg', '.jpeg', '.png')]) # TODO Will throw file error if _ocr directory of manga exists, but the manga directory doesnt

        else:
            self.path = pathlib.Path(path_in)
            if self.ocr:
                self.ocr_dir = os.path.join(os.path.join(path_in, "_ocr/"), self.path.name)
            else:
                self.image_files = natsorted([image_path for image_path in self.path.iterdir() if
                                              image_path.is_file() and image_path.suffix.lower() in (
                                                  '.jpg', '.jpeg', '.png')])
                # self.image_files[p_i] = natsorted([x for x in p_i.iterdir() if x.is_file() and x.suffix.lower() in ('.jpg', '.jpeg', '.png')])
                print(self.image_files)

    def create_image_pdf(self, render_mode=3):
        if self.conversion_type == "parent":

            for image_path in self.image_files:

                images = [
                    Image.open(f)
                    for f in self.image_files[image_path]
                ]

                pdf_path = f"{self.output_path}/{image_path.name}-temp.pdf"

                images[0].save(
                    pdf_path, "PDF", save_all=True, append_images=images[1:]
                )

                # error appears to be the resolution of the images
                '''
                doc = fitz.open()

                font = fitz.Font("cjk", language="ja")
                red = (1, 0, 0)
                '''
                if self.ocr:
                    doc = fitz.open(pdf_path)
                    font = fitz.Font("cjk", language="ja")
                    red = (1, 0, 0)

                    counter = 0
                    page = doc[counter]

                    for f in self.image_files[image_path]:
                        with open(os.path.join(os.path.join(self.ocr_dir, image_path.name),
                                               f'{f.stem}.json')) as ocr_file:
                            data = json.loads(ocr_file.read())

                        '''
                        img = fitz.open(f)  # open image

                        rect = img[0].rect  # pic dimension
                        pdfbytes = img.convert_to_pdf()  # make a PDF stream
                        img.close()  # no longer needed

                        imgPDF = fitz.open("pdf", pdfbytes)  # open stream as PDF
                        page = doc.new_page(width = rect.width,  # new page with ...
                                           height = rect.height)  # pic dimension
                        page.insert_image(rect, filename=f)  # image fills the page
                        '''

                        # Insert text
                        page.insert_font(fontname="F0", fontbuffer=font.buffer)
                        tw = fitz.TextWriter(page.rect)

                        shape = page.new_shape()

                        '''
                        img = open(f, "rb").read()  # make pic stream
                        doc.embfile_add(f.name, img,  filename=f.name,  # and embed it
                                            ufilename=f.name, desc=f.name
                                        )
                        '''

                        for block in data["blocks"]:
                            for index, line in enumerate(block["lines"]):

                                fontsize = block['font_size'] * (0.8)
                                # print(fitz.get_text_length(line, fontsize=fontsize))

                                point = fitz.Point(block["lines_coords"][index][0][0],
                                                   block["lines_coords"][index][0][1] + fontsize)

                                text = line

                                #rect = fitz.Rect(block["lines_coords"][index][0][0], block["lines_coords"][index][0][1],
                                #                 block["lines_coords"][index][2][0], block["lines_coords"][index][2][1])
                                #shape.draw_rect(rect)
                                #shape.finish(color=red, width=0.3)

                                if block["vertical"]:
                                    tw.appendv(
                                        point,
                                        text,
                                        font=font,
                                        fontsize=fontsize,
                                        language="ja"
                                    )
                                else:
                                    tw.append(
                                        point,
                                        text,
                                        font=font,
                                        fontsize=fontsize,
                                        language="ja"
                                    )

                                shape.commit()

                        tw.write_text(page, render_mode=render_mode)

                        counter += 1
                        try:
                            page = doc[counter]
                        except:
                            break

                    doc.save(f"{self.output_path}/{image_path.name}.pdf")
                    os.remove(pdf_path)
        else:
            images = [
                Image.open(f)
                for f in self.image_files
            ]

            pdf_path = f"{self.output_path}/{image_path.name}-temp.pdf"

            images[0].save(
                pdf_path, "PDF", save_all=True, append_images=images[1:]
            )


if __name__ == "__main__":
    path = input("Enter path: ")
    if path:
        test_converter = PDFGenerator(path, "child", "./output", ocr=False)
        test_converter.create_image_pdf(render_mode=0)
    else:
        test_converter = PDFGenerator('../../../dev_test/example/pdf/', "parent", "../../../dev_test/output")
        test_converter.create_image_pdf(render_mode=0)
