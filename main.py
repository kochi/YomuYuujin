import sys
import traceback

from PyQt6.QtCore import QObject, pyqtSignal, QThread, QRunnable, pyqtSlot, QThreadPool
from PyQt6.QtWidgets import QApplication, QDialog, QMainWindow, QPushButton, QFileDialog, QMessageBox
from app.ui import Ui_MainWindow

from app.utilities.pdf_generator import PDFGenerator
from app.utilities.ocr_generator import mokuro
from os.path import dirname, basename

# basedir = os.path.dirname(__file__)

try:
    from ctypes import windll  # Only exists on Windows.

    myappid = 'kochi.yomuyuujin.1'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
except ImportError:
    pass


class WorkerSignals(QObject):
    """
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        tuple (exctype, value, traceback.format_exc() )

    result
        object data returned from processing, anything

    progress
        int indicating % progress

    """
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(object)


class Worker(QRunnable):
    """
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    """

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        """
        Initialise the runner function with passed args, kwargs.
        """

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


class MainWindow(QMainWindow, Ui_MainWindow):
    """Main window."""

    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)
        self.setupUi(self)
        self.button_logic()
        self.progress_text.setReadOnly(True)
        self.progress_text.setAcceptRichText(True)

        self.input_path = None
        self.save_file = None
        self.force_cpu = False

        self.threadpool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())

    def button_logic(self):
        self.folder_button.clicked.connect(self.folder_selection)
        self.archive_button.clicked.connect(self.file_selection)
        self.output_button.clicked.connect(self.output_selection)

        self.create_button.clicked.connect(self.create_selected)
        self.create_button.setEnabled(False)

        self.force_cpu_button.stateChanged.connect(self.force_cpu_changed)

    def folder_selection(self):
        selected_folder = QFileDialog.getExistingDirectory()
        if selected_folder:
            self.input_path = selected_folder
            self.folder_button.setText(basename(selected_folder))

            if self.input_path and self.save_file:
                self.create_button.setEnabled(True)
            print(selected_folder)
        else:
            print('no folder selected')

    def file_selection(self):
        selected_file = QFileDialog.getOpenFileName(filter="Comic Files (*.cbz *.zip)")[0]
        if selected_file:
            self.input_path = selected_file
            self.archive_button.setText(basename(selected_file))

            if self.input_path and self.save_file:
                self.create_button.setEnabled(True)
            print(selected_file)
        else:
            print('no file selected')

    def output_selection(self):
        # selected_file = QFileDialog.getSaveFileName(filter="PDF File (*.pdf)")[0]
        selected_file = QFileDialog.getExistingDirectory()
        if selected_file:
            self.save_file = selected_file
            self.output_button.setText(basename(selected_file))

            if self.input_path and self.save_file:
                self.create_button.setEnabled(True)
            print(selected_file)
        else:
            print('no save selected')

    def force_cpu_changed(self):
        if self.force_cpu_button.isChecked():
            self.force_cpu = True
        else:
            self.force_cpu = False

    def create_pdf(self, input_path, output_path, progress_callback):
        # TODO - CHECK FOR WHAT HAPPENS WHEN MOKURO RUNS INTO AN ARCHIVE AND ENSURE PATH IS STILL RIGHT
        try:
            progress_callback.emit('Starting OCR...')
            mokuro(input_path, qt_progress_callback=progress_callback, force_cpu=self.force_cpu)
            progress_callback.emit('Creating PDF...')
            test_converter = PDFGenerator(dirname(input_path), "parent", output_path)
            test_converter.create_image_pdf()
            progress_callback.emit(f'<p><span style="color: green;">Completed!</span>')
        except Exception as e:
            progress_callback.emit(
                f'<p><span style="color: red;">An error was encountered: {e}. Please report this error</span>')
            QMessageBox.critical(
                self,
                "Something went wrong",
                f"Error: {e}"
            )

    def progress_fn(self, t):
        self.progress_text.append(str(t))

    def create_selected(self):
        worker = Worker(self.create_pdf, self.input_path, self.save_file)
        worker.signals.progress.connect(self.progress_fn)

        worker.signals.finished.connect(
            lambda: QMessageBox.information(
                self,
                "Completed!",
                "OCR & PDF Creation Completed!"
            ))

        self.threadpool.start(worker)


if __name__ == "__main__":
    # Create the application
    app = QApplication(sys.argv)
    # Create and show the application's main window
    win = MainWindow()
    win.show()
    # Run the application's main loop
    sys.exit(app.exec())
