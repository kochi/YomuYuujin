

# 読む友人
おっす！

This is an app to help learn Japanese and read raw manga. 

The app uses OCR to detect characters in the manga and then creates a PDF with selectable text so that definitions can be found using a dictionary such as Yomichan or the device dictionary (such as on Kindle/Kobo)

# Installation
Builds for all platforms coming soon!

For now some builds are available [here](https://mega.nz/folder/KiwwARJL#yB5D8m2fkG3eOAQ_W3VKLw)

# Running Locally
* Download project `git clone https://codeberg.org/kochi/YomuYuujin`
* Install requirements `pip install -r requirements.txt`
* Run main.py `python main.py`

# Roadmap
* M1/M2 GPU utilisation
* Manga downloader
* Kindle to Anki importer

# Credits
Lots of code was adapted and used from the following projects.
* https://github.com/kha-white/mokuro
* https://github.com/kha-white/manga-ocr
* https://github.com/dmMaze/comic-text-detector

Icon image from [irasutoya](https://www.irasutoya.com)

YomuYuujin is hosted on [codeberg.org](https://codeberg.org/kochi/YomuYuujin).

<a href="https://codeberg.org/kochi/YomuYuujin">
    <img alt="Get it on Codeberg" src="https://get-it-on.codeberg.org/get-it-on-blue-on-white.png" height="60">
</a>